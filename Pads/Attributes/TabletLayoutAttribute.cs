﻿using System;
using Tablet.Core.Models;

namespace Tablet.Core.Attributes
{
	[AttributeUsage(AttributeTargets.Class)]
	public class TabletLayoutAttribute : Attribute
	{
		public TabletLayoutAttribute ()
		{
		}

		public LayoutPosition ChildLayoutPosition { get; set; }
		public Type ChildViewModelType { get; set; }
	}
}
