﻿using System;
namespace Tablet.Core.Attributes
{
	[AttributeUsage (AttributeTargets.Class)]
	public class TabletChildAttribute : Attribute
	{
		public TabletChildAttribute (Type parentViewModel, bool addToBackStack)
		{
			ParentViewModel = parentViewModel;
			AddToBackStack = addToBackStack;
		}
		public Type ParentViewModel { get; private set; }
		public bool AddToBackStack { get; private set; }
	}
}
