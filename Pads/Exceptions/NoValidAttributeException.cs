﻿using System;
namespace Tablet.Core.Exceptions
{
	public class NoValidAttributeException : Exception
	{
		public NoValidAttributeException () : this ("No avaiable attribute on View Model")
		{
		}

		public NoValidAttributeException (string message) : base (message)
		{
		}

		public NoValidAttributeException (string message, Exception exception) : base (message, exception)
		{
		}
	}
}
