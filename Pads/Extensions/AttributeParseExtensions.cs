﻿using System;
using System.Linq;
using System.Reflection;
using Tablet.Core.Exceptions;

namespace Tablet.Core.Extensions
{
	public static class AttributeParseExtensions
	{
		public static bool IsCustomAttributed<TAttribute> (this System.Reflection.PropertyInfo properyInfo)
		{
			return properyInfo.GetCustomAttributes ()?.OfType<TAttribute> ().Any () ?? false;
		}

		public static TAttribute GetCutomAttribute<TAttribute> (this System.Reflection.PropertyInfo properyInfo) where TAttribute : Attribute
		{
			var attrs = properyInfo.GetCustomAttributes ();
			if (attrs?.Any () ?? false) {
				return attrs.Cast<TAttribute> ()?.FirstOrDefault ();
			}
			return null;
		}

		public static bool IsCustomAttributed<TAttribute> (this Type viewModelType)
		{
			return viewModelType.GetTypeInfo ().GetCustomAttributes()?.OfType<TAttribute> ().Any () ?? false;
		}


		public static TAttribute GetCutomAttribute<TAttribute> (this Type viewModelType) where TAttribute : Attribute
		{
			var attrs = viewModelType.GetTypeInfo ().GetCustomAttributes ();
			if (attrs.Any ()) {
				return attrs.Cast<TAttribute> ()?.FirstOrDefault ();
			}
			return default (TAttribute);
		}
	}
}
