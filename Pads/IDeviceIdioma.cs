﻿namespace Tablet.Core
{
	public interface IDeviceIdioma
	{
		bool IsTablet { get; }
	}
}
