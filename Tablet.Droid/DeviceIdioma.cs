﻿using System;
using Android.App;
using Android.Util;
using Tablet.Core;

namespace Tablet.Droid
{
	public class DeviceIdioma : IDeviceIdioma
	{
		bool _isTablet;
		public bool IsTablet {
			get {
				DisplayMetrics dm = Application.Context.Resources.DisplayMetrics;
				float screenWidth = dm.WidthPixels / dm.Xdpi;
				float screenHeight = dm.HeightPixels / dm.Ydpi;
				double size = Math.Sqrt (Math.Pow (screenWidth, 2) +
										Math.Pow (screenHeight, 2));

				_isTablet = size >= 6;
				return _isTablet;
			}
		}
	}
}
