﻿using System;
namespace Tablet.Droid
{
	public static class Constants
	{
		public static readonly string TabletLayoutCountTag = "_tabletLayoutCountTag";
		public static readonly string TabletFragmentBundleKey = "_tabletFragmentBundleTag";
		public static readonly string TabletLeftFragmentBundleTag = "_tabletLeftFragmentBundleTag";
		public static readonly string TabletRightFragmentBundleTag = "_tabletRightFragmentBundleTag";
	}
}
