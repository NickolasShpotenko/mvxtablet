﻿using System;
using Android.OS;
using Android.Runtime;
using Android.Views;
using MvvmCross.Binding.Droid.BindingContext;
using MvvmCross.Core.ViewModels;
using MvvmCross.Droid.Support.V4;
using MvvmCross.Platform.Exceptions;

namespace Tablet.Droid.Views
{
	[Register ("tablet.droid.views.MvxTabletFragmentBase")]
	public class MvxTabletFragmentBase : MvxFragment
	{
		protected const string FragmentRestoreContentBundleKey = "__fragmentRestoreContentBundleKey";

		public MvxTabletFragmentBase () : base ()
		{
		}

		public MvxTabletFragmentBase (IMvxViewModel viewModel, int contentId)
		{
			ContentId = contentId;
			ViewModel = viewModel;
		}

		protected MvxTabletFragmentBase (IntPtr javaReference, Android.Runtime.JniHandleOwnership transfer) : base (javaReference, transfer)
		{
		}

		public override IMvxViewModel ViewModel {
			get {
				return base.ViewModel;
			}
			set {
				base.ViewModel = value;
			}
		}

		public int ContentId { get; private set; }

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			ContentId = ContentId == 0 ? savedInstanceState?.GetInt (FragmentRestoreContentBundleKey, 0) ?? 0 : ContentId;
			if (ContentId == 0) {
				throw new MvxException ("There is no content id for fragment , please provide your activty with MvxTabletChild attribute");
			}
			base.OnCreateView (inflater, container, savedInstanceState);
			var view = this.BindingInflate (ContentId, null);
			return view;
		}

		public override void OnViewModelSet ()
		{
			base.OnViewModelSet ();
		}

		public override void OnSaveInstanceState (Bundle outState)
		{
			base.OnSaveInstanceState (outState);
			outState.PutInt (FragmentRestoreContentBundleKey, ContentId);
		}

		protected virtual string GetUniqueTag ()
		{
			return ViewModel.ToString () + this.ToString ();
		}
	}
}
