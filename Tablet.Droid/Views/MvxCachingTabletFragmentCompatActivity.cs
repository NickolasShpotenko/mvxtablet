﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Util;
using Android.Views;
using MvvmCross.Platform;
using MvvmCross.Platform.Exceptions;
using MvvmCross.Platform.Platform;
using MvvmCross.Binding.Droid.BindingContext;
using MvvmCross.Droid.Platform;
using MvvmCross.Droid.Views;
using MvvmCross.Core.ViewModels;
using MvvmCross.Core.Views;
using MvvmCross.Droid.Shared.Caching;
using MvvmCross.Droid.Shared.Presenter;
using MvvmCross.Droid.Shared.Attributes;
using MvvmCross.Droid.Shared.Fragments;
using MvvmCross.Droid.Support.V7.AppCompat;
using Android.Widget;
using Tablet.Droid.Models;
using Tablet.Core.Attributes;
using Tablet.Core.Extensions;
using Tablet.Droid.Attributes;
using Tablet.Droid.Interfaces;

namespace Tablet.Droid.Views
{
	[Register ("tablet.droid.views.MvxCachingTabletFragmentCompatActivity")]
	public abstract class MvxCachingTabletFragmentCompatActivity : MvxAppCompatActivity, IFragmentCacheableActivity, IMvxFragmentHost , IMvxTabletHost
	{
		public const string ViewModelRequestBundleKey = "__mvxViewModelRequest";
		private const string SavedFragmentTypesKey = "__mvxSavedFragmentTypes";
		private const int LeftId = 111;
		private const int RightId = 222;
		private const int FullId = 333;
		private IFragmentCacheConfiguration _fragmentCacheConfiguration;

		private IMvxViewsContainer _container;

		public override View OnCreateView (View parent, string name, Context context, IAttributeSet attrs)
		{
			var view = MvxAppCompatActivityHelper.OnCreateView (parent, name, context, attrs);
			return view ?? base.OnCreateView (parent, name, context, attrs);
		}

		protected IMvxViewsContainer Container {
			get { return _container ?? (_container = Mvx.Resolve<IMvxViewsContainer> ()); }
		}

		protected enum FragmentReplaceMode
		{
			NoReplace,
			ReplaceFragment,
			ReplaceViewModel,
			ReplaceFragmentAndViewModel
		}

		protected MvxCachingTabletFragmentCompatActivity ()
		{
		}

		protected MvxCachingTabletFragmentCompatActivity (IntPtr javaReference, JniHandleOwnership transfer)
			: base (javaReference, transfer)
		{ }

		private static void RestoreViewModelsFromBundle (IMvxJsonConverter serializer, Bundle savedInstanceState)
		{
			IMvxSavedStateConverter savedStateConverter;
			IMvxMultipleViewModelCache viewModelCache;
			IMvxViewModelLoader viewModelLoader;

			if (!Mvx.TryResolve (out savedStateConverter)) {
				Mvx.Trace ("Could not resolve IMvxSavedStateConverter, won't be able to convert saved state");
				return;
			}

			if (!Mvx.TryResolve (out viewModelCache)) {
				Mvx.Trace ("Could not resolve IMvxMultipleViewModelCache, won't be able to convert saved state");
				return;
			}

			if (!Mvx.TryResolve (out viewModelLoader)) {
				Mvx.Trace ("Could not resolve IMvxViewModelLoader, won't be able to load ViewModel for caching");
				return;
			}

			// Harder ressurection, just in case we were killed to death.
			var json = savedInstanceState.GetString (SavedFragmentTypesKey);
			if (string.IsNullOrEmpty (json)) return;

			var savedState = serializer.DeserializeObject<Dictionary<string, Type>> (json);
			foreach (var item in savedState) {
				var bundle = savedInstanceState.GetBundle (item.Key);
				if (bundle.IsEmpty) continue;

				var mvxBundle = savedStateConverter.Read (bundle);
				var request = MvxViewModelRequest.GetDefaultRequest (item.Value);

				// repopulate the ViewModel with the SavedState and cache it.
				var vm = viewModelLoader.LoadViewModel (request, mvxBundle);
				viewModelCache.Cache (vm, item.Key);
			}
		}

		private void RestoreFragmentsCache ()
		{
			// See if Fragments were just sleeping, and repopulate the _lookup (which is accesed in GetFragmentInfoByTag)
			// with references to them.

			// we do not want to restore fragments which aren't tracked by our cache
			foreach (var fragment in GetCurrentCacheableFragments ()) {
				// if used tag is proper tag such that:
				// it is unique and immutable
				// and fragment is properly registered
				// then there must be exactly one matching value in _lookup fragment cache container
				var fragmentTag = GetTagFromFragment (fragment);
				var fragmentInfo = GetFragmentInfoByTag (fragmentTag);

				fragmentInfo.CachedFragment = fragment as IMvxFragmentView;
			}
		}

		private Dictionary<string, Type> CreateFragmentTypesDictionary (Bundle outState)
		{
			IMvxSavedStateConverter savedStateConverter;
			if (!Mvx.TryResolve (out savedStateConverter)) {
				return null;
			}

			var typesForKeys = new Dictionary<string, Type> ();

			var currentFragsInfo = GetCurrentCacheableFragmentsInfo ();
			foreach (var info in currentFragsInfo) {
				var fragment = info.CachedFragment as IMvxFragmentView;
				if (fragment == null)
					continue;

				var mvxBundle = fragment.CreateSaveStateBundle ();
				var bundle = new Bundle ();
				savedStateConverter.Write (bundle, mvxBundle);
				outState.PutBundle (info.Tag, bundle);

				if (!typesForKeys.ContainsKey (info.Tag))
					typesForKeys.Add (info.Tag, info.ViewModelType);
			}

			return typesForKeys;
		}

		protected virtual void ReplaceFragment (FragmentTransaction ft, IMvxCachedFragmentInfo fragInfo)
		{
			ft.Replace (fragInfo.ContentId, fragInfo.CachedFragment as Android.Support.V4.App.Fragment, fragInfo.Tag);
		}

		protected override void OnSaveInstanceState (Bundle outState)
		{
			base.OnSaveInstanceState (outState);
			IMvxJsonConverter ser;
			if (FragmentCacheConfiguration.HasAnyFragmentsRegisteredToCache && Mvx.TryResolve (out ser)) {
				FragmentCacheConfiguration.SaveFragmentCacheConfigurationState (outState, ser);

				var typesForKeys = CreateFragmentTypesDictionary (outState);
				if (typesForKeys == null)
					return;

				var json = ser.SerializeObject (typesForKeys);
				outState.PutString (SavedFragmentTypesKey, json);
			}
		}

		protected virtual void ShowFragment (string tag, int contentId, Bundle bundle, bool forceAddToBackStack = false, bool forceReplaceFragment = false)
		{
			IMvxCachedFragmentInfo fragInfo;
			FragmentCacheConfiguration.TryGetValue (tag, out fragInfo);

			IMvxCachedFragmentInfo currentFragInfo = null;
			var currentFragment = SupportFragmentManager.FindFragmentById (contentId);

			if (currentFragment != null)
				FragmentCacheConfiguration.TryGetValue (currentFragment.Tag, out currentFragInfo);

			if (fragInfo == null)
				throw new MvxException ("Could not find tag: {0} in cache, you need to register it first.", tag);

			// We shouldn't replace the current fragment unless we really need to.
			FragmentReplaceMode fragmentReplaceMode = FragmentReplaceMode.ReplaceFragmentAndViewModel;
			if (!forceReplaceFragment)
				fragmentReplaceMode = ShouldReplaceCurrentFragment (fragInfo, currentFragInfo, bundle);

			if (fragmentReplaceMode == FragmentReplaceMode.NoReplace)
				return;

			var ft = SupportFragmentManager.BeginTransaction ();
			OnBeforeFragmentChanging (fragInfo, ft);

			fragInfo.ContentId = contentId;

			//If we already have a previously created fragment, we only need to send the new parameters
			if (fragInfo.CachedFragment != null && fragmentReplaceMode == FragmentReplaceMode.ReplaceFragment) {
				(fragInfo.CachedFragment as Android.Support.V4.App.Fragment).Arguments.Clear ();
				(fragInfo.CachedFragment as Android.Support.V4.App.Fragment).Arguments.PutAll (bundle);
			} else {
				//Otherwise, create one and cache it
				fragInfo.CachedFragment = Android.Support.V4.App.Fragment.Instantiate (this, FragmentJavaName (fragInfo.FragmentType),
					bundle) as IMvxFragmentView;
				OnFragmentCreated (fragInfo, ft);
			}

			currentFragment = fragInfo.CachedFragment as Android.Support.V4.App.Fragment;
			ft.Replace (fragInfo.ContentId, fragInfo.CachedFragment as Android.Support.V4.App.Fragment, fragInfo.Tag);

			//if replacing ViewModel then clear the cache after the fragment
			//has been added to the transaction so that the Tag property is not null
			//and the UniqueImmutableCacheTag property (if not overridden) has the correct value
			if (fragmentReplaceMode == FragmentReplaceMode.ReplaceFragmentAndViewModel) {
				var cache = Mvx.GetSingleton<IMvxMultipleViewModelCache> ();
				cache.GetAndClear (fragInfo.ViewModelType, GetTagFromFragment (fragInfo.CachedFragment as Android.Support.V4.App.Fragment));
			}

			if ((currentFragment != null && fragInfo.AddToBackStack) || forceAddToBackStack) {
				ft.AddToBackStack (fragInfo.Tag);
			}

			OnFragmentChanging (fragInfo, ft);
			ft.Commit ();
			SupportFragmentManager.ExecutePendingTransactions ();
			OnFragmentChanged (fragInfo);
		}

		/// <summary>
		///     Show Fragment with a specific tag at a specific placeholder
		/// </summary>
		/// <param name="tag">The tag for the fragment to lookup</param>
		/// <param name="contentId">Where you want to show the Fragment</param>
		/// <param name="bundle">Bundle which usually contains a Serialized MvxViewModelRequest</param>
		/// <param name="forceAddToBackStack">If you want to force add the fragment to the backstack so on backbutton it will go back to it. Note: This will override IMvxCachedFragmentInfo.AddToBackStack configuration.</param>
		/// <param name="forceReplaceFragment">If you want the fragment to be re-created</param>
		protected virtual void ShowFragment (string tag, int parentContentId, int contentId, Bundle bundle, IMvxViewModel viewModel, bool forceAddToBackStack = false, bool forceReplaceFragment = false)
		{
			IMvxCachedFragmentInfo fragInfo;
			FragmentCacheConfiguration.TryGetValue (tag, out fragInfo);

			IMvxCachedFragmentInfo currentFragInfo = null;
			var currentFragment = SupportFragmentManager.FindFragmentById (contentId);

			if (currentFragment != null)
				FragmentCacheConfiguration.TryGetValue (currentFragment.Tag, out currentFragInfo);

			if (fragInfo == null)
				throw new MvxException ("Could not find tag: {0} in cache, you need to register it first.", tag);

			// We shouldn't replace the current fragment unless we really need to.
			FragmentReplaceMode fragmentReplaceMode = FragmentReplaceMode.ReplaceFragmentAndViewModel;
			if (!forceReplaceFragment)
				fragmentReplaceMode = ShouldReplaceCurrentFragment (fragInfo, currentFragInfo, bundle);

			if (fragmentReplaceMode == FragmentReplaceMode.NoReplace)
				return;

			var ft = SupportFragmentManager.BeginTransaction ();
			OnBeforeFragmentChanging (fragInfo, ft);

			fragInfo.ContentId = parentContentId;

			//If we already have a previously created fragment, we only need to send the new parameters
			if (fragInfo.CachedFragment != null && fragmentReplaceMode == FragmentReplaceMode.ReplaceFragment) {
				(fragInfo.CachedFragment as Android.Support.V4.App.Fragment)?.Arguments?.Clear ();
				(fragInfo.CachedFragment as Android.Support.V4.App.Fragment)?.Arguments?.PutAll (bundle);
			} else {
				//Otherwise, create one and cache it
				fragInfo.CachedFragment = new MvxTabletFragmentBase (viewModel, contentId);
				OnFragmentCreated (fragInfo, ft);
			}

			currentFragment = fragInfo.CachedFragment as Android.Support.V4.App.Fragment;
			ft.Replace (fragInfo.ContentId, fragInfo.CachedFragment as Android.Support.V4.App.Fragment, fragInfo.Tag);

			//if replacing ViewModel then clear the cache after the fragment
			//has been added to the transaction so that the Tag property is not null
			//and the UniqueImmutableCacheTag property (if not overridden) has the correct value
			if (fragmentReplaceMode == FragmentReplaceMode.ReplaceFragmentAndViewModel) {
				var cache = Mvx.GetSingleton<IMvxMultipleViewModelCache> ();
				cache.GetAndClear (fragInfo.ViewModelType, GetTagFromFragment (fragInfo.CachedFragment as Android.Support.V4.App.Fragment));
			}

			if ((currentFragment != null && fragInfo.AddToBackStack) || forceAddToBackStack) {
				ft.AddToBackStack (fragInfo.Tag);
			}

			OnFragmentChanging (fragInfo, ft);
			ft.Commit ();
			SupportFragmentManager.ExecutePendingTransactions ();
			OnFragmentChanged (fragInfo);
		}

		protected virtual void AddFragment (FragmentTransaction ft, string tag, int parentContentId, int contentId, IMvxViewModel viewModel, Bundle bundle)
		{
			var viewModelType = viewModel.GetType ();
			FragmentCacheConfiguration.RegisterFragmentToCache(tag,typeof(MvxTabletFragmentBase),viewModelType);
			IMvxCachedFragmentInfo fragInfo;
			FragmentCacheConfiguration.TryGetValue (tag, out fragInfo);

			IMvxCachedFragmentInfo currentFragInfo = null;
			var currentFragment = SupportFragmentManager.FindFragmentById (contentId);

			if (currentFragment != null)
				FragmentCacheConfiguration.TryGetValue (currentFragment.Tag, out currentFragInfo);

			if (fragInfo == null)
				throw new MvxException ("Could not find tag: {0} in cache, you need to register it first.", tag);

			// We shouldn't replace the current fragment unless we really need to.
			FragmentReplaceMode fragmentReplaceMode = FragmentReplaceMode.ReplaceFragmentAndViewModel;

			if (fragmentReplaceMode == FragmentReplaceMode.NoReplace)
				return;
			
			fragInfo.ContentId = parentContentId;

			//If we already have a previously created fragment, we only need to send the new parameters
			if (fragInfo.CachedFragment != null && fragmentReplaceMode == FragmentReplaceMode.ReplaceFragment) {
				(fragInfo.CachedFragment as Android.Support.V4.App.Fragment).Arguments.Clear ();
				(fragInfo.CachedFragment as Android.Support.V4.App.Fragment).Arguments.PutAll (bundle);
			} else {
				//Otherwise, create one and cache it
				//TODO create viewmodel automatically 
				fragInfo.CachedFragment = new MvxTabletFragmentBase (viewModel, contentId);
				OnFragmentCreated (fragInfo, ft);
			}

			currentFragment = fragInfo.CachedFragment as Android.Support.V4.App.Fragment;
			ft.Replace (parentContentId, currentFragment, tag);

			//if replacing ViewModel then clear the cache after the fragment
			//has been added to the transaction so that the Tag property is not null
			//and the UniqueImmutableCacheTag property (if not overridden) has the correct value
			if (fragmentReplaceMode == FragmentReplaceMode.ReplaceFragmentAndViewModel) {
				var cache = Mvx.GetSingleton<IMvxMultipleViewModelCache> ();
				cache.GetAndClear (fragInfo.ViewModelType, GetTagFromFragment (fragInfo.CachedFragment as Android.Support.V4.App.Fragment));
			}

			if (currentFragment != null && fragInfo.AddToBackStack) {
				ft.AddToBackStack (fragInfo.Tag);
			}

			OnFragmentChanging (fragInfo, ft);
		}

		protected virtual FragmentReplaceMode ShouldReplaceCurrentFragment (IMvxCachedFragmentInfo newFragment, IMvxCachedFragmentInfo currentFragment, Bundle replacementBundle)
		{
			var oldBundle = (newFragment.CachedFragment as Android.Support.V4.App.Fragment)?.Arguments;
			if (oldBundle == null) return FragmentReplaceMode.ReplaceFragment;

			var serializer = Mvx.Resolve<IMvxNavigationSerializer> ();

			var json = oldBundle.GetString (MvxFragmentsPresenter.ViewModelRequestBundleKey);
			var oldRequest = serializer.Serializer.DeserializeObject<MvxViewModelRequest> (json);
			if (oldRequest == null) return FragmentReplaceMode.ReplaceFragment;

			json = replacementBundle.GetString (MvxFragmentsPresenter.ViewModelRequestBundleKey);
			var replacementRequest = serializer.Serializer.DeserializeObject<MvxViewModelRequest> (json);
			if (replacementRequest == null) return FragmentReplaceMode.ReplaceFragment;

			var areParametersEqual = ((oldRequest.ParameterValues == replacementRequest.ParameterValues) ||
				(oldRequest.ParameterValues.Count == replacementRequest.ParameterValues.Count &&
					!oldRequest.ParameterValues.Except (replacementRequest.ParameterValues).Any ()));

			if (currentFragment?.Tag != newFragment.Tag) {
				return !areParametersEqual
					? FragmentReplaceMode.ReplaceFragmentAndViewModel
						: FragmentReplaceMode.ReplaceFragment;
			} else
				return !areParametersEqual
					? FragmentReplaceMode.ReplaceFragmentAndViewModel
						: FragmentReplaceMode.NoReplace;
		}

		public override void OnBackPressed ()
		{
			if (SupportFragmentManager.BackStackEntryCount >= 1) {
				SupportFragmentManager.PopBackStackImmediate ();

				if (FragmentCacheConfiguration.EnableOnFragmentPoppedCallback) {
					//NOTE(vvolkgang) this is returning ALL the frags. Should we return only the visible ones?
					var currentFragsInfo = GetCurrentCacheableFragmentsInfo ();
					OnFragmentPopped (currentFragsInfo);
				}

				return;
			}

			base.OnBackPressed ();
		}

		protected virtual List<IMvxCachedFragmentInfo> GetCurrentCacheableFragmentsInfo ()
		{
			return GetCurrentCacheableFragments ()
					.Select (frag => GetFragmentInfoByTag (GetTagFromFragment (frag)))
					.ToList ();
		}

		protected virtual IEnumerable<Android.Support.V4.App.Fragment> GetCurrentCacheableFragments ()
		{
			var currentFragments = SupportFragmentManager.Fragments ?? Enumerable.Empty<Android.Support.V4.App.Fragment> ();
			var fragments = currentFragments
				.Where (fragment => fragment != null);
				return currentFragments
				.Where (fragment => fragment != null);
				// we are not interested in fragments which are not supposed to cache!
				//.Where (fragment => fragment.GetType ().IsFragmentCacheable (GetType ()));
		}

		protected virtual IMvxCachedFragmentInfo GetLastFragmentInfo ()
		{
			var currentCacheableFragments = GetCurrentCacheableFragments ().ToList ();
			if (!currentCacheableFragments.Any ())
				throw new InvalidOperationException ("Cannot retrieve last fragment as FragmentManager is empty.");

			var lastFragment = currentCacheableFragments.Last ();
			var tagFragment = GetTagFromFragment (lastFragment);

			return GetFragmentInfoByTag (tagFragment);
		}

		protected virtual string GetTagFromFragment (Android.Support.V4.App.Fragment fragment)
		{
			var mvxFragmentView = fragment as IMvxFragmentView;

			// ReSharper disable once PossibleNullReferenceException
			// Fragment can never be null because registered fragment has to inherit from IMvxFragmentView
			return mvxFragmentView.UniqueImmutableCacheTag;
		}

		private LinearLayout CreateLayout (int count)
		{
			//TODO create child fragmentmanager
			LinearLayout linearLayout = new LinearLayout (this);
			LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams (
				ViewGroup.LayoutParams.MatchParent,
				ViewGroup.LayoutParams.MatchParent);
			linearLayout.Orientation = Orientation.Horizontal;
			linearLayout.SetBackgroundColor (Android.Graphics.Color.Aqua);
			linearLayout.LayoutParameters = llp;

			FrameLayout leftFrameLayout = new FrameLayout (this);
			leftFrameLayout.Id = LeftId;
			var flp = new LinearLayout.LayoutParams (
				0,
				ViewGroup.LayoutParams.MatchParent);
			flp.Weight = 1;
			leftFrameLayout.LayoutParameters = flp;
			linearLayout.AddView (leftFrameLayout);

			FrameLayout rightFrameLayout = new FrameLayout (this);
			rightFrameLayout.Id = RightId;
			flp = new LinearLayout.LayoutParams (
				0,
				ViewGroup.LayoutParams.MatchParent);
			flp.Weight = 1;
			rightFrameLayout.LayoutParameters = flp;
			linearLayout.AddView (rightFrameLayout);

			return linearLayout;
		}

		protected abstract int ContentId { get; }

		//public sealed override void SetContentView (View view)
		//{
		//	throw new MvxException ("You can't set content view, because it already setted");
		//}

		public sealed override void SetContentView (int layoutResId)
		{
			if (!IsTablet) {
				base.SetContentView (layoutResId);
			}
		}

		protected bool IsTablet
		{
			get { return Intent.Extras?.GetBundle (Constants.TabletFragmentBundleKey) != null; }
		}

		//public sealed override void SetContentView (View view, ViewGroup.LayoutParams @params)
		//{
		//	throw new MvxException ("You can't set content view, because it already setted");
		//}

		protected virtual void SetLayout ()
		{
			if (!IsTablet) {
				SetContentView (ContentId);
			} else {
				var bundleData = Intent.Extras?.GetBundle (Constants.TabletFragmentBundleKey);
				SetContentView (CreateLayout (bundleData.GetInt (Constants.TabletLayoutCountTag, 1)));
			}          
		}

		protected override void OnCreate (Bundle bundle)
		{
			// Prevents crash when activity in background with history enable is reopened after 
			// Android does some auto memory management.
			var setup = MvxAndroidSetupSingleton.EnsureSingletonAvailable (this);
			setup.EnsureInitialized ();

			base.OnCreate (bundle);
			SetLayout ();
			if (bundle == null) {
				if (IsTablet) {
					var bundleData = Intent.Extras?.GetBundle (Constants.TabletFragmentBundleKey);
					HandleTabletFragmentsPresentation (bundleData);
				} else {
					HandleIntent (Intent);
				}
			} else {
				IMvxJsonConverter serializer;
				if (!Mvx.TryResolve (out serializer)) {
					Mvx.Trace (
						"Could not resolve IMvxJsonConverter, it is going to be hard to create ViewModel cache");
					return;
				}

				FragmentCacheConfiguration.RestoreCacheConfiguration (bundle, serializer);
				// Gabriel has blown his trumpet. Ressurect Fragments from the dead.
				RestoreFragmentsCache ();

				RestoreViewModelsFromBundle (serializer, bundle);
			}
		}

		protected virtual void HandleTabletFragmentsPresentation (Bundle bundle)
		{
			if (bundle == null) return;

			var requestDataString = bundle.GetString (Constants.TabletFragmentBundleKey, string.Empty);
			if (string.IsNullOrEmpty (requestDataString)) return;

			var requestData = Newtonsoft.Json.JsonConvert.DeserializeObject<MvxTabletFragmentData> (requestDataString);

			var ft = SupportFragmentManager.BeginTransaction ();

			if (requestData.TabletLayout.ChildViewModelType != null) {
				var viewType = Container.GetViewType (requestData.TabletLayout.ChildViewModelType);

				var childData = viewType.GetCutomAttribute<MvxTabletChildAttribute> ();
				if (childData == null) {
					throw new MvxException ($"Could not find content id for {childData}, check if you add {nameof (MvxTabletChildAttribute)} to activity");
				}

				var childViewModel = Mvx.Resolve<IMvxViewModelLoader> ().LoadViewModel (MvxViewModelRequest.GetDefaultRequest (requestData.TabletLayout.ChildViewModelType), null);
				AddFragment (ft, GetFragmentTag (requestData.TabletLayout.ChildViewModelType), (int) requestData.TabletLayout.ChildLayoutPosition, childData.ContentId, childViewModel, bundle);;
			}


			AddFragment (ft, GetFragmentTag(requestData.Request.ViewModelType), LeftId, ContentId, ViewModel, bundle);


         	ft.Commit ();
			SupportFragmentManager.ExecutePendingTransactions ();

		}

		protected override void OnNewIntent (Intent intent)
		{
			base.OnNewIntent (intent);

			HandleIntent (intent);
		}

		protected virtual void HandleIntent (Intent intent)
		{
			var fragmentRequestText = intent.Extras?.GetString (ViewModelRequestBundleKey);
			if (fragmentRequestText == null)
				return;

			var converter = Mvx.Resolve<IMvxNavigationSerializer> ();
			var fragmentRequest = converter.Serializer.DeserializeObject<MvxViewModelRequest> (fragmentRequestText);

			var mvxAndroidViewPresenter = Mvx.Resolve<IMvxAndroidViewPresenter> ();
			mvxAndroidViewPresenter.Show (fragmentRequest);
		}

		/// <summary>
		/// Close Fragment with a specific tag at a specific placeholder
		/// </summary>
		/// <param name="tag">The tag for the fragment to lookup</param>
		/// <param name="contentId">Where you want to close the Fragment</param>
		protected virtual void CloseFragment (string tag, int contentId)
		{
			var frag = SupportFragmentManager.FindFragmentById (contentId);
			if (frag == null) return;

			SupportFragmentManager.PopBackStackImmediate (tag, 1);
		}

		protected virtual string FragmentJavaName (Type fragmentType)
		{
			return Java.Lang.Class.FromType (fragmentType).Name;
		}

		public virtual void OnBeforeFragmentChanging (IMvxCachedFragmentInfo fragmentInfo, FragmentTransaction transaction)
		{
		}

		// Called before the transaction is commited
		public virtual void OnFragmentChanging (IMvxCachedFragmentInfo fragmentInfo, FragmentTransaction transaction) { }

		public virtual void OnFragmentChanged (IMvxCachedFragmentInfo fragmentInfo)
		{
		}

		public virtual void OnFragmentPopped (IList<IMvxCachedFragmentInfo> currentFragmentsInfo)
		{
		}

		public virtual void OnFragmentCreated (IMvxCachedFragmentInfo fragmentInfo, FragmentTransaction transaction)
		{
		}

		protected virtual IMvxCachedFragmentInfo GetFragmentInfoByTag (string tag)
		{
			IMvxCachedFragmentInfo fragInfo;
			FragmentCacheConfiguration.TryGetValue (tag, out fragInfo);

			if (fragInfo == null)
				throw new MvxException ("Could not find tag: {0} in cache, you need to register it first.", tag);
			return fragInfo;
		}

		public IFragmentCacheConfiguration FragmentCacheConfiguration => _fragmentCacheConfiguration ?? (_fragmentCacheConfiguration = BuildFragmentCacheConfiguration ());

		public virtual IFragmentCacheConfiguration BuildFragmentCacheConfiguration ()
		{
			return new DefaultFragmentCacheConfiguration ();
		}

		public virtual bool Show (MvxViewModelRequest request, Type parentType, bool addToBackStack)
		{
			var tag = GetFragmentTag (request);
			FragmentCacheConfiguration.RegisterFragmentToCache (tag, typeof (MvxTabletFragmentBase), request.ViewModelType, addToBackStack);

			var viewType = Container.GetViewType (request.ViewModelType);

			var childData = viewType.GetCutomAttribute<MvxTabletChildAttribute> ();
			if (childData == null) {
				throw new MvxException ($"Could not find content id for {childData}, check if you add {nameof (MvxTabletChildAttribute)} to activity");
			}
			var viewModel = Mvx.Resolve<IMvxViewModelLoader> ().LoadViewModel (MvxViewModelRequest.GetDefaultRequest (request.ViewModelType), null);
			ShowFragment (tag, GetParentContentId (parentType), childData.ContentId, Bundle.Empty, viewModel, addToBackStack);
			return true;
		}

		private int GetParentContentId (Type parentType)
		{
			var tag = GetFragmentTag (parentType);
			IMvxCachedFragmentInfo fragInfo;
			FragmentCacheConfiguration.TryGetValue (tag, out fragInfo);
			if (fragInfo == null) {
				throw new MvxException ("Could not find cached fragment.", tag);
			}
			return fragInfo.ContentId;
		}

		private string GetFragmentTag (Type viewModelType)
		{
			return $"{typeof(MvxTabletFragmentBase)}{viewModelType.ToString ()}";
		}

		private string GetFragmentTag (MvxViewModelRequest request)
		{
			return $"{typeof (MvxTabletFragmentBase)}{request.ViewModelType.ToString ()}";
		}

		public virtual bool Show (MvxViewModelRequest request, Bundle bundle, Type fragmentType, MvxFragmentAttribute fragmentAttribute)
		{
			var fragmentTag = GetFragmentTag (request);
			FragmentCacheConfiguration.RegisterFragmentToCache (fragmentTag, fragmentType, request.ViewModelType, fragmentAttribute.AddToBackStack);

			ShowFragment (fragmentTag, fragmentAttribute.FragmentContentId, bundle);
			return true;
		}

		public virtual bool Close (IMvxViewModel viewModel)
		{
			if (SupportFragmentManager.BackStackEntryCount == 0) {
				base.OnBackPressed ();
				return true;
			}

			//Workaround for closing fragments. This will not work when showing multiple fragments of the same viewmodel type in one activity
			var frag = GetCurrentCacheableFragmentsInfo ().FirstOrDefault (x => x.ViewModelType == viewModel.GetType ());
			if (frag == null) {
				return false;
			}

			// Close method can not be fully fixed at this moment. That requires some changes in main MvvmCross library
			CloseFragment (frag.Tag, frag.ContentId);
			return true;
		}
	}

	public abstract class MvxCachingTabletFragmentCompatActivity<TViewModel>
		: MvxCachingTabletFragmentCompatActivity
	, IMvxAndroidView<TViewModel> where TViewModel : class, IMvxViewModel
	{
		protected MvxCachingTabletFragmentCompatActivity (IntPtr ptr, JniHandleOwnership ownership) : base (ptr, ownership)
		{

		}

		protected MvxCachingTabletFragmentCompatActivity ()
		{

		}
		public new TViewModel ViewModel {
			get { return (TViewModel)base.ViewModel; }
			set { base.ViewModel = value; }
		}
	}
}