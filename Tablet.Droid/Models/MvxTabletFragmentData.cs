﻿using System;
using Android.OS;
using MvvmCross.Core.ViewModels;
using MvvmCross.Droid.Shared.Attributes;
using Tablet.Core.Attributes;

namespace Tablet.Droid.Models
{
	public class MvxTabletFragmentData
	{
		public MvxTabletFragmentData (TabletLayoutAttribute tabletLayout, MvxViewModelRequest request, Type childType)
		{
			TabletLayout = tabletLayout;
			Request = request;
			FragmentType = childType;
		}

		public TabletLayoutAttribute TabletLayout { get; private set; }
		public MvxViewModelRequest Request { get; private set; }
		public Type FragmentType { get; private set; }
	}
}
