﻿using System;
namespace Tablet.Droid.Attributes
{
	[AttributeUsage(AttributeTargets.Class)]
	public class MvxTabletChildAttribute : Attribute
	{
		public MvxTabletChildAttribute (int contentId)
		{
			ContentId = contentId;
		}

		public int ContentId { get; private set; }
	}
}
