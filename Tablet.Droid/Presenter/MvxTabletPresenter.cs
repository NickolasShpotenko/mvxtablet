﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Android.OS;
using MvvmCross.Core.ViewModels;
using MvvmCross.Droid.Shared.Attributes;
using MvvmCross.Platform;
using MvvmCross.Platform.Droid.Platform;
using Tablet.Core;
using Tablet.Core.Attributes;
using Tablet.Core.Extensions;
using Tablet.Droid.Interfaces;
using Tablet.Droid.Models;

namespace Tablet.Droid.Presenter
{
	public class MvxTabletPresenter : MvxFragmentsPresenter
	{
		public const string ViewModelTabletRequestBundleKey = "__viewModelTabletRequestBundleKey";

		public MvxTabletPresenter (IEnumerable<Assembly> androidViewAssemblies) : base (androidViewAssemblies)
		{
			DeviceIdioma = new DeviceIdioma ();
		}

		public IDeviceIdioma DeviceIdioma { get; set; }

		public override void Show (MvxViewModelRequest request)
		{
			if (DeviceIdioma.IsTablet) {
				ShowTabletView (request);
			} else {
				base.Show (request);
			}
		}

		protected virtual void ShowTabletView (MvxViewModelRequest request)
		{
			if (request.ViewModelType.IsCustomAttributed<TabletLayoutAttribute> ()) {
				ShowTabletActivity (request);
				return;
			}
			if (request.ViewModelType.IsCustomAttributed<TabletChildAttribute> ()) {
				ShowTabletFragment (request);
				return;
			}
			base.Show (request);
		}

		protected virtual void ShowTabletFragment (MvxViewModelRequest request)
		{
			var bundle = new Bundle ();
			var attributeData = request.ViewModelType.GetCutomAttribute<TabletChildAttribute> ();
			var serializedRequest = Serializer.Serializer.SerializeObject (request);
			bundle.PutString (ViewModelRequestBundleKey, serializedRequest);
			GetActualTabletHost ().Show (request, attributeData.ParentViewModel, attributeData.AddToBackStack);
		}

		protected virtual void ShowTabletActivity (MvxViewModelRequest request)
		{
			var attributeData = request.ViewModelType.GetCutomAttribute<TabletLayoutAttribute> ();

			var childType = attributeData.ChildViewModelType;

			var tabletFragmentData = new MvxTabletFragmentData (attributeData, request, childType);

			ShowTabletActivity (tabletFragmentData);
		}

		protected virtual void ShowTabletActivity (MvxTabletFragmentData data)
		{
			var bundle = new Bundle ();
			var intent = CreateIntentForRequest (data.Request);
			if (data != null) {
				bundle.PutString (Constants.TabletFragmentBundleKey,  Newtonsoft.Json.JsonConvert.SerializeObject (data));
				bundle.PutInt (Constants.TabletLayoutCountTag, data.TabletLayout.ChildViewModelType != null ? 2 : 1);
				intent.PutExtra (Constants.TabletFragmentBundleKey, bundle);
			}
			Show (intent);

		}

		protected IMvxTabletHost GetActualTabletHost ()
		{
			var currentActivity = Mvx.Resolve<IMvxAndroidCurrentTopActivity> ().Activity;
			var tabletHost = currentActivity as IMvxTabletHost;

			if (tabletHost == null)
				throw new InvalidOperationException ($"You are trying to close or show ViewModel associated with Tablet when currently top Activity ({currentActivity.GetType ()} does not implement IMvxTableHost interface!");

			return tabletHost;
		}
	}
}
