﻿using System;
using MvvmCross.Core.ViewModels;

namespace Tablet.Droid.Interfaces
{
	public interface IMvxTabletHost
	{
		bool Close (IMvxViewModel viewModel);
		bool Show (MvxViewModelRequest request, Type parentFragmnet, bool addToBackStack);
	}
}
