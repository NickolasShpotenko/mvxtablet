using MvvmCross.Core.ViewModels;
using Tablet.Core.Attributes;

namespace TabletTest.ViewModels
{
	[TabletChildAttribute(typeof(SecondViewModel), true)]
    public class ThirdViewModel : MvxViewModel
    {
        private string _hello = "Hello MvvmCross";
        public string Hello
        { 
            get { return _hello; }
            set { SetProperty (ref _hello, value); }
        }
    }
}
