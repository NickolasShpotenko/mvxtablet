using MvvmCross.Core.ViewModels;
using Tablet.Core.Attributes;

namespace TabletTest.ViewModels
{
	[TabletLayoutAttribute(ChildLayoutPosition = Tablet.Core.Models.LayoutPosition.Right,ChildViewModelType = typeof(SecondViewModel))]
    public class FirstViewModel : MvxViewModel
    {
        string _buttonText = "Go to second";
        public string ButtonText
        {
            get { return _buttonText; }
        }

        MvxCommand _clickCommand;
        public IMvxCommand ClickCommand
        {
            get
            {
                return _clickCommand ?? (_clickCommand = new MvxCommand(() => ShowViewModel<SecondViewModel>()));
            }
        }


		protected override void SaveStateToBundle (IMvxBundle bundle)
		{
			base.SaveStateToBundle (bundle);

		}

		public new void SaveState (IMvxBundle state)
		{
			base.SaveState (state);
		}

    }
}
