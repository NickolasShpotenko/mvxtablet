using MvvmCross.Platform.Plugins;

namespace TabletTest.Droid.Bootstrap
{
    public class JsonPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.Json.PluginLoader>
    {
    }
}