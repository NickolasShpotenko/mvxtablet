using Android.App;
using Android.OS;
using MvvmCross.Droid.Views;
using Tablet.Droid.Attributes;
using TabletTest.ViewModels;

namespace TabletTest.Droid.Views
{
	[MvxTabletChildAttribute (Resource.Layout.ThirdView)]
    [Activity(Label = "View for ThirdViewModel", Name = "tablettest.droid.views.ThirdView")]
    public class ThirdView : MvxActivity<ThirdViewModel>
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.ThirdView);
        }
    }
}
