using Android.App;
using Android.OS;
using MvvmCross.Droid.Views;
using Tablet.Droid.Views;
using TabletTest.ViewModels;

namespace TabletTest.Droid.Views
{
	[Activity(Label = "View for FirstViewModel",Name = "tablettest.droid.views.FirstView")]
    public class FirstView : MvxCachingTabletFragmentCompatActivity<FirstViewModel>
    {
		protected override int ContentId {
			get {
				return Resource.Layout.FirstView;
			}
		}
		
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.FirstView);
        }
    }
}
