using Android.App;
using Android.OS;
using MvvmCross.Droid.Views;
using Tablet.Droid.Attributes;
using TabletTest.ViewModels;

namespace TabletTest.Droid.Views
{
	[MvxTabletChildAttribute(Resource.Layout.SecondView)]
    [Activity(Label = "View for SecondViewModel", Name = "tablettest.droid.views.SecondView")]
    public class SecondView : MvxActivity<SecondViewModel>
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.SecondView);
        }
    }
}